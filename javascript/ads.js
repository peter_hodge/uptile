var ADS = {};

// TODO - document this method
ADS.GetPostData = function(element) {
    var control_values = [];

    // look for all input/textarea/select elements and combine them into an object
    $(element).find('input').each(function() {
        var input = $(this);
        var input_type = input.attr('type');

        // if the input is disabled, ignore it
        if (!input.attr('disabled')) {
            if (input_type === 'text'
                || input_type === 'hidden'
                || input_type === 'password'
                // html5 input types
                || input_type === 'color'
                || input_type === 'date'
                || input_type === 'datetime'
                || input_type === 'datetime-local'
                || input_type === 'email'
                || input_type === 'month'
                || input_type === 'number'
                || input_type === 'range'
                || input_type === 'tel'
                || input_type === 'time'
                || input_type === 'url'
                || input_type === 'week'
                ) {
                // add the input's name and value to the list for processing
                control_values.push(input.attr('name'), String(input.attr('value')));
            }
            if (input_type === 'radio' || input_type === 'checkbox') {
                // only use the input's name and value if it has the 'checked' property
                if (input.attr('checked'))
                    control_values.push(input.attr('name'), String(input.attr('value')));
            }
        }
    });
    $(element).find('select').each(function() {
        var select = $(this);

        // if the select box is disabled, ignore it
        if (!select.attr('disabled')) {
            // add the input's name and value to the list for processing
            control_values.push(select.attr('name'), String(select.val()));
        }
    });
    $(element).find('textarea').each(function() {
        var textarea = $(this);

        // if the textarea is disabled, ignore it
        if (!textarea.attr('disabled')) {
            // add the control's name and value to the list for processing
            control_values.push(textarea.attr('name'), String(textarea.val()));
        }
    });

    // NOTE: valid control names are allowed in the following formats:
    //   name
    //   name[]
    //   name[0]
    //   name[key]
    //   name[key][]
    //   name[key][0]
    //   name[key][key]
    //   name[0][...]
    //   name[][...]
    //
    // {name} and {key} are allowed the characters a-z, A-Z, 0-9 and '_.-+'
    //
    // Names with formats other than the above *might* work, but they are not guaranteed.

    // process control_values list and combine into a complex object
    var post_data = {};
    for (var i = 0; i < control_values.length; ++i) {
        var control_name  = control_values[i][0];
        var control_value = control_values[i][1];

        // split on all '[' characters
        var name_parts = control_name.split('[');
        var store = post_data;

        // grab the first name part
        var target = name_parts[0];

        // descend into store using the name parts as keys
        for (var i = 1; i < name_parts.length; ++i)
        {
            // for all but the first part, grab everything before the first ']'
            var part = name_parts[i].substring(0, name_parts[i].indexOf(']'));

            // if the name part is empty
            if (part === '') {
                // if our target store doesn't exist, we can make it a list
                if (typeof store[target] == 'undefined') {
                    store[target] = [ undefined ];
                    // update pointers to pointer deepder into the store object
                    store = store[target];
                    target = 0;
                    continue;
                }

                // if it already is a list (which should be the case if the programmer used sensible field names), then
                // we can point at the index of the next item
                if (store[target].constructor == Array) {
                    store = store[target];
                    target = store.length;
                    continue;
                }

                // in all other cases, we just treat the name part '' as if it is a normal dictionary key
            }

            // if the target store is already set to a value of some sort, we need to replace it with a generic
            // object
            if (typeof store[target] == 'string')
                store[target] = {};

            // and now that store[target] is an object, we can update the pointers to descend depper into it
            store = store[target];
            target = '';
        }

        // so now we can store our value in the store (which might be a pointer deeper into post_data)
        store[target] = control_value;
    }

    // our data object is now constructed
    return post_data;
};

ADS.GetControlByName = function(control_name) {
    // TODO - implement and document this function
};

// if you want to implement all Atom methods from scratch, use ADS.Atom as
// your prototype
ADS.Atom = function() { };

// if you want just the standard methods available, use ADS.AtomStandard as your prototype
ADS.AtomStandard = function() {
    this.atom_SetID = function(new_id) {
        this.atom_element.attr('id', new_id);
    };

    this.atom_GetID = function() {
        return this.atom_element.attr('id');
    };

    this.atom_AddClass = function(class_name) {
        this.atom_element.addClass(class_name);
    };

    this.atom_RemoveClass = function(class_name) {
        this.atom_element.removeClass(class_name);
    };

    this.atom_SetContentHTML = function(html_or_atom) {
        if (html_or_atom instanceof ADS.Atom)
            html_or_atom = html_or_atom.atom_GetHTML();
        this.atom_element.html(html_or_atom);
    };

    this.atom_SetContentText = function(text) {
        this.atom_element.text(text);
    };

    this.atom_GetContentHTML = function() {
        return this.atom_element.html();
    };

    this.atom_GetHTML = function() {
        return this.atom_element[0].outerHTML;
    };

    this.atom_GetJQObject = function() {
        return this.atom_element;
    };

    // DISPLAY METHODS
    
    // helper function to set the display/position values
    function __setDisplayPositionFloat(element, new_display, new_position, new_float) {
        element.style.position = new_position;
        element.style.cssFloat = new_float;
        if (element.style.display != 'none') {
            // if the atom is visible, set its display attribute
            element.style.display = new_display;
        } else {
            // otherwise, remember the attribute for later
            if (new_display)
                element.setAttribute('data-atom-display', new_display);
            else
                element.removeAttribute('data-atom-display');
        }
    }
    // helper function to set new coordinates for top/bottom/left/right
    function __setCoords(element, new_top, new_right, new_bottom, new_left) {
        element.style.top    = typeof new_top    == 'undefined' ? 'auto' : new_top;
        element.style.bottom = typeof new_bottom == 'undefined' ? 'auto' : new_bottom;
        element.style.right  = typeof new_right  == 'undefined' ? 'auto' : new_right;
        element.style.left   = typeof new_left   == 'undefined' ? 'auto' : new_left;
    }

    this.atom_DrawBlock = function(new_top, new_right, new_bottom, new_left) {
        // any arguments not provided default to 'auto'
        // use 'relative' and set positional css properties
        __setDisplayPositionFloat(this.atom_element[0], 'block', 'relative', 'none');
        __setCoords(this.atom_element[0], new_top, new_right, new_bottom, new_left);
    };

    this.atom_DrawInline = function(new_top, new_right, new_bottom, new_left) {
        // any arguments not provided default to 'auto'
        __setDisplayPositionFloat(this.atom_element[0], 'inline-block', 'relative', 'none');
        __setCoords(this.atom_element[0], new_top, new_right, new_bottom, new_left);
    };

    this.atom_DrawAbsolute = function(new_top, new_right, new_bottom, new_left) {
        // any arguments not provided default to 'auto'
        __setDisplayPositionFloat(this.atom_element[0], 'block', 'absolute', 'none');
        __setCoords(this.atom_element[0], new_top, new_right, new_bottom, new_left);
    };

    this.atom_DrawFixed = function(new_top, new_right, new_bottom, new_left) {
        __setDisplayPositionFloat(this.atom_element[0], 'block', 'fixed', 'none');
        __setCoords(this.atom_element[0], new_top, new_right, new_bottom, new_left);
    };

    this.atom_DrawFloat = function(direction, new_top, new_right, new_bottom, new_left) {
        // any arguments not provided default to 'auto'
        __setDisplayPositionFloat(this.atom_element[0], 'block', 'relative', direction == 'right' ? 'right' : 'left');
        __setCoords(this.atom_element[0], new_top, new_right, new_bottom, new_left);
    };

    this.atom_DrawCustom = function(new_display, new_position, new_float, new_top, new_right, new_bottom, new_left) {
        // any arguments not provided default to empty string so that the page's CSS rules will apply
        if (typeof new_display == 'undefined' || new_display === 'none')
            new_display = '';
        if (typeof new_position == 'undefined')
            new_position = '';
        if (typeof new_float == 'undefined')
            new_float = '';
        if (typeof new_top == 'undefined')
            new_top = '';
        if (typeof new_right == 'undefined')
            new_right = '';
        if (typeof new_bottom == 'undefined')
            new_bottom = '';
        if (typeof new_left == 'undefined')
            new_left = '';

        __setDisplayPositionFloat(this.atom_element[0], new_display, new_position, new_float);
        __setCoords(this.atom_element[0], new_top, new_right, new_bottom, new_left);
    };

    this.atom_Hide = function() {
        // remember the old 'display' value
        var old_display = this.atom_element[0].style.display;
        if (old_display != 'none' && old_display)
            this.atom_element[0].setAttribute('data-atom-display', old_display);
        // set display: none;
        this.atom_element[0].style.display = 'none';
    };

    this.atom_Show = function() {
        var element = this.atom_element[0];
        if (element.style.display == 'none') {
            // check the data-atom-display attribute to see what value to use
            var new_display = element.getAttribute('data-atom-display');
            if (new_display) {
                element.style.display = new_display;
            } else {
                // if no display value was set, use empty string to revert back to CSS rules
                element.style.display = '';
            }
            element.removeAttribute('data-atom-display');
        }
    };

    this.atom_SetHeight = function(new_height) {
        this.atom_element[0].style.height = new_height;
    };

    this.atom_SetHeightRange = function(min_height, max_height) {
        this.atom_element[0].style.minHeight = min_height;
        this.atom_element[0].style.maxHeight = max_height;
    };

    this.atom_SetWidth = function(new_width) {
        this.atom_element[0].style.width = new_width;
    };

    this.atom_SetWidthRange = function(min_width, max_width) {
        this.atom_element[0].style.minWidth = min_width;
        this.atom_element[0].style.maxWidth = max_width;
    };

    this.atom_SetMargin = function(new_top, new_right, new_bottom, new_left) {
        this.atom_element[0].style.marginTop    = new_top;
        this.atom_element[0].style.marginRight  = new_right;
        this.atom_element[0].style.marginBottom = new_bottom;
        this.atom_element[0].style.marginLeft   = new_left;
    };

    this.atom_SetClear = function(new_clear) {
        this.atom_element[0].style.clear = new_clear;
    };
};

// if you want to include the Control Atom methods as well, use
// ADS.AtomControl as your prototype
ADS.AtomControl = function() {
    // TODO - finish adding all methods
};

ADS.AtomStandard.prototype = new ADS.Atom();
ADS.AtomControl.prototype  = new ADS.AtomStandard();

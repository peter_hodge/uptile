<?php

class ADS_Atom {
}

class ADS_Atom_Standard extends ADS_Atom {
    private $__id       = false;
    private $__classes  = array();

    private $__style    = array();
    private $__visible  = true;

    private $__html     = '';

    public function atom_SetID($new_id) {
        if ($new_id !== false)
            $new_id = trim($new_id);
        if (!strlen($new_id))
            $new_id = false;
        $this->__id = $new_id;
    }

    public function atom_GetID() {
        return $this->__id;
    }

    public function atom_AddClass($class_name) {
        assert(is_string($class_name));
        $class_name = trim($class_name);
        assert(strlen($class_name));
        if (!in_array($class_name, $this->__classes))
            $this->__classes[] = $class_name;
    }

    public function atom_RemoveClass($class_name) {
        assert(is_string($class_name));
        $class_name = trim($class_name);
        assert(strlen($class_name));

        if (false !== $idx = array_search($class_name, $this->__classes))
            $this->__classes[] = $class_name;
    }

    public function atom_GetClasses() {
        return $this->__classes;
    }

    public function atom_SetContentHTML($html_or_atom) {
        if ($html_or_atom instanceof ADS_Atom)
            $html_or_atom = $html_or_atom->atom_GetHTML();
        else
            assert(is_string($html_or_atom));
        $this->__html = $html_or_atom;
    }

    public function atom_SetContentText($text) {
        $this->__html = htmlentities($text);
    }

    public function atom_GetContentHTML() {
        return $this->__html;
    }


    // DISPLAY METHODS

    public function atom_DrawBlock($new_top = 'auto', $new_right = 'auto', $new_bottom = 'auto', $new_left = 'auto') {
        $this->__style['display']  = 'block';
        $this->__style['float']    = 'none';

        $this->__style['position'] = 'relative';
        $this->__style['top']      = $new_top;
        $this->__style['right']    = $new_right;
        $this->__style['bottom']   = $new_bottom;
        $this->__style['left']     = $new_left;
    }

    public function atom_DrawInline($new_top = 'auto', $new_right = 'auto', $new_bottom = 'auto', $new_left = 'auto') {
        $this->__style['display']  = 'inline-block';
        $this->__style['float']    = 'none';

        $this->__style['position'] = 'relative';
        $this->__style['top']      = $new_top;
        $this->__style['right']    = $new_right;
        $this->__style['bottom']   = $new_bottom;
        $this->__style['left']     = $new_left;
    }

    public function atom_DrawAbsolute($new_top = 'auto', $new_right = 'auto', $new_bottom = 'auto', $new_left = 'auto') {
        $this->__style['display']  = 'block';
        $this->__style['position'] = 'absolute';
        $this->__style['float']    = 'none';
        $this->__style['top']      = $new_top;
        $this->__style['right']    = $new_right;
        $this->__style['bottom']   = $new_bottom;
        $this->__style['left']     = $new_left;
    }

    public function atom_DrawFixed($new_top = 'auto', $new_right = 'auto', $new_bottom = 'auto', $new_left = 'auto') {
        $this->__style['display']  = 'block';
        $this->__style['position'] = 'fixed';
        $this->__style['float']    = 'none';
        $this->__style['top']      = $new_top;
        $this->__style['right']    = $new_right;
        $this->__style['bottom']   = $new_bottom;
        $this->__style['left']     = $new_left;
    }

    public function atom_DrawFloat($direction, $new_top = 'auto', $new_right = 'auto', $new_bottom = 'auto', $new_left = 'auto') {
        $this->__style['display']  = 'block';
        $this->__style['position'] = 'relative';
        $this->__style['top']      = $new_top;
        $this->__style['right']    = $new_right;
        $this->__style['bottom']   = $new_bottom;
        $this->__style['left']     = $new_left;
        $this->__style['float']    = $direction === 'right' ? 'right' : 'left';
        unset($this->__style['top'], $this->__style['right'], $this->__style['bottom'], $this->__style['left']);
    }

    public function atom_DrawCustom($new_display = '',
                                    $new_position = '',
                                    $new_float = '',
                                    $new_top = '',
                                    $new_right = '',
                                    $new_bottom = '',
                                    $new_left = '') {
        $this->__style['display']  = $new_display === 'none' ? '' : $new_display;
        $this->__style['position'] = $new_position;
        $this->__style['float']    = $new_float;
        $this->__style['top']      = $new_top;
        $this->__style['right']    = $new_right;
        $this->__style['bottom']   = $new_bottom;
        $this->__style['left']     = $new_left;
    }

    public function atom_Hide() {
        $this->__visible = false;
    }

    public function atom_Show() {
        $this->__visible = true;
    }

    public function atom_SetHeight($height) {
        $this->__style['height'] = $height;
    }

    public function atom_SetHeightRange($min_height, $max_height) {
        $this->__style['min-height'] = $min_height;
        $this->__style['max-height'] = $max_height;
    }

    public function atom_SetWidth($width) {
        $this->__style['width'] = $width;
    }

    public function atom_SetWidthRange($min_width, $max_width) {
        $this->__style['min-width'] = $min_width;
        $this->__style['max-width'] = $max_width;
    }

    public function atom_SetMargin($top, $right, $bottom, $left) {
        unset($this->__style['margin'],
              $this->__style['margin-left'],
              $this->__style['margin-right'],
              $this->__style['margin-top'],
              $this->__style['margin-bottom']);

        if ($top === '' && $right === '' && $bottom === '' && $left === '') {
            // no margins to set
        } else if ($top !== '' && $right !== '' && $bottom !== '' && $left !== '') {
            // if all margins have a value, we can use shortcut notation
            $this->__style['margin'] = "$top $right $bottom $left";
        } else {
            // need to specify individual css rules
            $this->__style['margin-top']    = $top;
            $this->__style['margin-right']  = $right;
            $this->__style['margin-bottom'] = $bottom;
            $this->__style['margin-left']   = $left;
        }
    }

    public function atom_SetClear($clear) {
        if ($clear === '')
            unset($this->__style['clear']);
        else
            $this->__style['clear'] = $clear;
    }

    public function atom_GetAttributesString() {
        // if the element is not visible, need to add display: none;
        if (!$this->__visible) {
            // if the element is not visible, remember the custom display value in a data attribute so it is restored by
            // atom_Show() later
            if (isset($this->__style['display']) && $this->__style['display'] !== '')
                $return[] = "data-atom-display=\"$this->__style['display']\"";
            $this->__style['display'] = 'none';
        }

        $css_rules = "";
        foreach ($this->__style as $attribute => $value)
            if ($value !== '')
                $css_rules .= "$attribute:$value;";

        // combine attributes into one string
        $return = array();

        // always include the name of the atom as one of the CSS classes
        // TODO - document this behaviour
        // TODO - document this whole method
        $classes = $this->__classes;
        array_unshift($classes, get_class($this));
        $return[] = "class=\"".htmlentities(implode(' ', $classes))."\"";

        if ($this->__id)
            $return[] = "id=\"".htmlentities($this->__id)."\"";
        if (strlen($css_rules))
            $return[] = "style=\"$css_rules\"";
        return implode(' ', $return);
    }
}

class ADS_Atom_Control extends ADS_Atom_Standard {
}

class ADS_Main {
    private $__atoms;
	private $__paths = array();

	// TODO - this needs to be documented
	public function ads_AddPath($path) {
		$this->__paths[] = $path;
	}
	
	public function ads_GetPaths() {
		return $this->__paths;
	}

    public function ads_LoadAtom($atom_name) {
        $this->_getAtomPHP($atom_name);
        $this->__atoms[] = $atom_name;
    }

    public function ads_GetAllCSS($params = null) {
        $return = array();
        foreach ($this->__atoms as $atom_name) {
            if ($css = $this->_getAtomCSS($atom_name, $params)) {
                $return[] = "/* $atom_name */";
                $return[] = $css;
                $return[] = "";
            }
        }
        return implode("\n", $return);
    }

    public function ads_GetAllJS() {
        $return = array();
        foreach ($this->__atoms as $atom_name) {
            if ($js = $this->_getAtomJS($atom_name)) {
                $return[] = "/* $atom_name */";
                $return[] = $js;
                $return[] = "";
            }
        }
        return implode("\n", $return);
    }

    // TODO - not documented
    public function ads_GetAtomStandalone(ADS_Atom $atom) {
        $atom_name = get_class($atom);
        $return = array();
        if ($js = $this->_getAtomJS($atom_name)) {
            $return[] = "<script type=\"text/javascript\">";
            $return[] = $js;
            $return[] = "</script>";
        }
        if ($css = $this->_getAtomCSS($atom_name)) {
            $return[] = "<script type=\"text/css\">";
            $return[] = $css;
            $return[] = "</script>";
        }
        $return[] = $atom->atom_GetHTML();

        return implode("\n", $return);
    }

    protected function _getAtomPHP($atom_name) {
		foreach ($this->__paths as $root) {
			$real_path = $root.'/'.str_replace('_', '/', $atom_name).'.atom.php';
			if (file_exists($real_path))
				require $real_path;
		}
    }

    protected function _getAtomCSS($atom_name, $params = null) {
		foreach ($this->__paths as $root) {
			$file = $root.'/'.str_replace('_', '/', $atom_name).'.atom.css';
			if (file_exists($file))
				return file_get_contents($file);
		}
		return false;
    }

    protected function _getAtomJS($atom_name) {
		foreach ($this->__paths as $root) {
			$file = $root.'/'.str_replace('_', '/', $atom_name).'.atom.js';
			if (file_exists($file))
				return file_get_contents($file);
		}
		return false;
    }
}
